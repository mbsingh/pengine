package com.mbsingh.pengine.models.io

import com.mbsingh.pengine.controller.LoggingController
import com.mbsingh.pengine.models.objects.Project
import java.io.*
import java.nio.file.Paths
import java.util.*

private val SER_FILE_LOC = Paths.get(System.getProperty("user.dir"), ".pengine.ser").toString()
private val logger = LoggingController.logger

fun readFromLocalStorage(): LinkedList<Project> {

    logger.info("Starting read from serialized object")
    val serFile = File(SER_FILE_LOC)
    var projects = LinkedList<Project>()
    if (!serFile.exists()) {
        logger.warning("$SER_FILE_LOC not found. Returning new object.")
        return projects
    }

    lateinit var fInpStream: FileInputStream
    lateinit var objInpStream: ObjectInputStream
    try {
        fInpStream = FileInputStream(serFile)
        objInpStream = ObjectInputStream(fInpStream)

        val readObject = objInpStream.readObject()
        if (readObject is List<*>) {
            logger.info("Successfully read serialized object.")
            @Suppress("UNCHECKED_CAST")
            projects = readObject as? LinkedList<Project> ?: LinkedList()
        } else {
            logger.severe("$SER_FILE_LOC was found but failed to map to object.")
        }

    } catch (ioExcObj: IOException) {
        ioExcObj.printStackTrace()
        logger.severe("${ioExcObj.message} occurred while reading from $SER_FILE_LOC")
    } catch (noClsExcObj: ClassNotFoundException) {
        noClsExcObj.printStackTrace()
        logger.severe("${noClsExcObj.message} occurred while reading from $SER_FILE_LOC")
    } finally {
        objInpStream.close()
        fInpStream.close()
    }
    logger.info("Read from serialized object complete. Returning.")
    return projects
}

fun writeToLocalStorage(projects: List<Project>): Boolean {

    logger.info("Starting write to serialized object")
    val serFile = File(SER_FILE_LOC)
    val bkpLoc = "${SER_FILE_LOC}_bk"
    val bkpFile = File(bkpLoc)

    lateinit var fOutStream: FileOutputStream
    lateinit var objOutStream: ObjectOutputStream
    try {
        if (bkpFile.exists())
            logger.info("$bkpLoc exists. Delete result = ${bkpFile.delete()}")

        fOutStream = FileOutputStream(bkpLoc)
        objOutStream = ObjectOutputStream(fOutStream)

        objOutStream.writeObject(projects)
        logger.info("Object written to disk")
    } catch (ioExcObj: IOException) {
        ioExcObj.printStackTrace()
        logger.severe("${ioExcObj.message} occurred while writing to $bkpLoc")
    } finally {
        objOutStream.close()
        fOutStream.close()
    }

    var isSuccess = (bkpFile.exists())
    logger.finest("Written object found on disk? = $isSuccess")
    if (isSuccess) {
        serFile.delete()
        isSuccess = bkpFile.renameTo(serFile)
    }
    logger.info("Backup object file rename to primary operation result = $isSuccess. Returning.")
    return isSuccess

}