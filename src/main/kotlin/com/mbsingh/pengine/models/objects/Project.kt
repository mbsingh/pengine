package com.mbsingh.pengine.models.objects

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleListProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import tornadofx.*

class Project(projectTitle: String, isEffortEnabled: Boolean, description: String = "", stages: List<ProjectStage>, isComplete: Boolean = false) {

    val projectTitleProperty = SimpleStringProperty(projectTitle)
    var projectTitle: String by projectTitleProperty

    val isEffortEnabledProperty = SimpleBooleanProperty(isEffortEnabled)
    var isEffortEnabled by isEffortEnabledProperty

    val descriptionProperty = SimpleStringProperty(description)
    var description: String by descriptionProperty

    val stagesProperty = SimpleListProperty<ProjectStage>(FXCollections.observableList(stages))
    var stages by stagesProperty

    val isCompleteProperty = SimpleBooleanProperty(isComplete)
    var isComplete by isCompleteProperty

}

class ProjectModel: ItemViewModel<Project>() {

    val projectTitle = bind(Project::projectTitleProperty)
    val isEffortEnabled = bind(Project::isEffortEnabledProperty)
    val description = bind(Project::descriptionProperty)
    val stages: SimpleListProperty<ProjectStage> = bind(Project::stagesProperty)
    val isComplete = bind(Project::isCompleteProperty)

}

