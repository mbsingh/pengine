package com.mbsingh.pengine.models.objects

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import java.time.LocalDate
import tornadofx.*

class ProjectStage(stageTitle: String, estimatedEffortHours: Int = 0, effortHoursSpent: Int = 0, description: String = "", dueBy: LocalDate,
                   isComplete: Boolean = false) {

    val stageTitleProperty = SimpleStringProperty(stageTitle)
    var stageTitle: String by stageTitleProperty

    val estimatedEffortHoursProperty = SimpleIntegerProperty(estimatedEffortHours)
    var estimatedEffortHours by estimatedEffortHoursProperty

    val effortHoursSpentProperty = SimpleIntegerProperty(effortHoursSpent)
    var effortHoursSpent by effortHoursSpentProperty

    val descriptionProperty = SimpleStringProperty(description)
    var description: String by descriptionProperty

    val dueByProperty = SimpleObjectProperty<LocalDate>(dueBy)
    var dueBy: LocalDate by dueByProperty

    val isCompleteProperty = SimpleBooleanProperty(isComplete)
    var isComplete: Boolean by isCompleteProperty
}

class ProjectStageModel: ItemViewModel<ProjectStage>() {

    val stageTitle = bind(ProjectStage::stageTitleProperty)
    val estimatedEffortHours = bind(ProjectStage::estimatedEffortHoursProperty)
    val effortHoursSpent = bind(ProjectStage::effortHoursSpentProperty)
    val description = bind(ProjectStage::descriptionProperty)
    val dueBy = bind(ProjectStage::dueByProperty)
    val isComplete = bind(ProjectStage::isCompleteProperty)

}