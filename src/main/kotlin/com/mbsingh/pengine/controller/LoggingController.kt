package com.mbsingh.pengine.controller

import tornadofx.*
import java.util.logging.FileHandler
import java.util.logging.Level
import java.util.logging.Logger
import java.util.logging.SimpleFormatter

object LoggingController: Controller() {

    private val logLevel = if (System.getenv("DEBUG")=="true") Level.FINEST else Level.INFO
    private val fHandler = FileHandler(".pengine.log", 1000000, 10, true)

    val logger = Logger.getLogger("pengine")!!

     init {
         fHandler.formatter = SimpleFormatter()
         logger.level = logLevel
         logger.useParentHandlers = false
         logger.addHandler(fHandler)
     }

}