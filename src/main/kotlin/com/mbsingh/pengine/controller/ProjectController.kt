package com.mbsingh.pengine.controller

import com.mbsingh.pengine.models.io.readFromLocalStorage
import com.mbsingh.pengine.models.objects.Project
import com.mbsingh.pengine.models.objects.ProjectStage
import javafx.collections.FXCollections
import tornadofx.*
import java.time.LocalDate
import kotlin.collections.ArrayList


class ProjectController: Controller() {

    val projects = FXCollections.observableArrayList<Project>()!!
    var projectStages = FXCollections.observableArrayList<ProjectStage>()!!

    init {
        for (eachProject in readFromLocalStorage())
            addProject(eachProject)
        addNewProject("Init", true)
    }

    fun addNewProject(projectTitle: String, isEffortEnabled: Boolean) = projects.add(Project(projectTitle, isEffortEnabled,
            stages = ArrayList()))

    private fun addProject(project: Project) = projects.add(project)

    fun deleteProject(project: Project) = projects.remove(project)

    fun initOpenProject(project: Project) {
        projectStages = project.stages
    }

    fun addNewStage(stageTitle: String, effortHours: Int = 0, dueBy: LocalDate) {
        projectStages.add(ProjectStage(stageTitle, effortHours, dueBy = dueBy))
    }

    fun deleteStage(stage: ProjectStage) = projectStages.remove(stage)

    fun fetchStageCountString(project: Project): String {
        var total: Int = 0
        var done: Int = 0
        for (eachStage in project.stages) {
            total++
            if (eachStage.isComplete)
                done++
        }
        return "$done $total"
    }
}
