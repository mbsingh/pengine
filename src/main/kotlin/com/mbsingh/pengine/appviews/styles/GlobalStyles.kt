package com.mbsingh.pengine.appviews.styles

import javafx.scene.effect.DropShadow
import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import tornadofx.*

class GlobalStyles : Stylesheet() {

    companion object {
        val add_new_button by cssclass()
        val list_label by cssclass()
        val list_view by cssclass()
        val buttons_hbox_border by cssclass()
        val description_edit by cssclass()
        val completed_label by cssclass()
    }

    init {
        add_new_button {
            effect = DropShadow()
        }

        list_label {
            fontWeight = FontWeight.NORMAL
            fontFamily = "Georgia"
            fontSize = 30.px
        }

        list_view {
            listCell {
                and (even) {
                    backgroundColor += Color.WHITE
                    textFill = Color.BLACK
                }
                and (odd) {
                    backgroundColor += c("e6e6e6")
                    textFill = Color.BLACK
                }
                and (selected) {
                    backgroundColor += Color.BLACK
                    textFill = Color.WHITE
                }
            }
            borderColor += box(c("039ed3"))
            borderWidth += box(top = 1.px, right = 0.px, left = 0.px, bottom = 1.px)
        }
        buttons_hbox_border {
            borderColor += box(c("039ed3"))
            borderWidth += box(top = 0.px, right = 0.px, left = 0.px, bottom = 1.px)
        }
        description_edit {
            fontSize = 15.px
            fontFamily = "Times New Roman"
        }
        completed_label {
            fontSize = 15.px
            fontWeight = FontWeight.BOLD
            textFill = Color.DARKGREEN
        }
    }
}