package com.mbsingh.pengine.appviews.project
import tornadofx.*

class ProjectView(projectName: String): View(projectName) {

    override val root = borderpane {
        top(ProjectDetailView::class)
        center(StageList::class)
        bottom(AddStageView::class)
    }

}