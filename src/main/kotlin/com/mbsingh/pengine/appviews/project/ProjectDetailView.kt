package com.mbsingh.pengine.appviews.project

import com.mbsingh.pengine.appviews.main.AllProjectsView
import com.mbsingh.pengine.appviews.styles.GlobalStyles
import com.mbsingh.pengine.controller.ProjectController
import com.mbsingh.pengine.models.objects.ProjectModel
import javafx.geometry.Pos
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import javafx.scene.text.Font
import tornadofx.*

class ProjectDetailView : View() {

    private val selectedProject: ProjectModel by inject()
    private val projectController: ProjectController by inject()

    override val root = vbox {
        hbox {
            label(selectedProject.projectTitle) {
                alignment = Pos.CENTER_LEFT
                prefHeight = 20.0
                font = Font.font(30.0)
                textFill = Color.BLACK
                hboxConstraints { marginLeft = 20.0 }
            }
            hbox {
                hgrow = Priority.ALWAYS
            }
            hbox {
                alignment = Pos.CENTER_RIGHT
                button {
                    val imgObj = Image("resources/icons/back.png")
                    val imgVwObj = ImageView(imgObj)
                    imgVwObj.fitWidth = 25.0
                    imgVwObj.fitHeight = 25.0
                    graphic = imgVwObj
                    hboxConstraints { marginRight = 15.0 }
                    action {
                        val index = projectController.projects.indexOf(selectedProject.item)
                        selectedProject.commit {
                            projectController.projects[index] = selectedProject.item
                        }
                        close()
                        AllProjectsView().openWindow()
                    }
                }
            }
        }
        textarea(selectedProject.description).addClass(GlobalStyles.description_edit).apply {
            promptText = "Description"
            vboxConstraints {
                prefHeight = 30.0
                marginLeft = 5.0
                marginRight = 5.0
            }
            bind(selectedProject.description)
        }
        borderpane {
            left {
                checkbox("Effort Tracking").apply {
                    isSelected = selectedProject.item.isEffortEnabled
                    borderpaneConstraints {
                        marginLeft = 20.0
                        marginTop = 5.0
                        marginBottom = 5.0
                    }
                    bind(selectedProject.isEffortEnabled)
                }
            }
            right {
                checkbox("Completed").apply {
                    isSelected = selectedProject.item.isComplete
                    borderpaneConstraints {
                        marginRight = 20.0
                        marginTop = 5.0
                        marginBottom = 5.0
                    }
                    bind(selectedProject.isComplete)
                }
            }
        }
    }
}
