package com.mbsingh.pengine.appviews.project

import com.mbsingh.pengine.appviews.main.AllProjectsView
import com.mbsingh.pengine.appviews.project.stage.AddHours
import com.mbsingh.pengine.appviews.project.stage.StageEditor
import com.mbsingh.pengine.appviews.styles.GlobalStyles
import com.mbsingh.pengine.controller.ProjectController
import com.mbsingh.pengine.models.objects.ProjectModel
import com.mbsingh.pengine.models.objects.ProjectStageModel
import javafx.scene.control.ButtonType
import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import tornadofx.*

class StageList : View() {

    private val projectsController: ProjectController by inject()
    private val selectedProject: ProjectModel by inject()
    private val selectedStage: ProjectStageModel by inject()

    override val root = vbox {
        minHeight = 50.0
        listview(projectsController.projectStages).addClass(GlobalStyles.list_view).apply {
            style {
                backgroundInsets += box(0.px)
            }
            onUserSelect {
                StageEditor(selectedStage.item.stageTitle).openModal()
            }
            cellFormat {
                graphic = borderpane {
                    center {
                        vbox {
                            label(it.stageTitle).addClass(GlobalStyles.list_label)
                            label(it.description) {
                                prefWidth = 500.0
                                prefHeight = 40.0
                                isWrapText = true
                                font = Font.font(13.0)
                            }
                        }
                    }
                    right {
                        vbox {
                            hbox {
                                label("Due by: ").apply {
                                    font = Font.font("Georgia", FontWeight.BOLD, 15.0)
                                    hboxConstraints { marginBottom = 2.0; marginTop = 2.0 }
                                }
                                label("${it.dueBy}").apply { hboxConstraints { marginBottom = 5.0; marginTop = 5.0 } }
                            }
                            hbox {
                                label("Effort hours remaining: ").apply {
                                    font = Font.font("Georgia", FontWeight.BOLD, 15.0)
                                    hboxConstraints { marginBottom = 2.0 }
                                    visibleWhen { selectedProject.isEffortEnabled }
                                }
                                label("${(it.estimatedEffortHours - it.effortHoursSpent)}").apply {
                                    hboxConstraints { marginBottom = 2.0 }
                                    visibleWhen { selectedProject.isEffortEnabled }
                                }
                            }
                            label("COMPLETED").addClass(GlobalStyles.completed_label).apply {
                                visibleWhen { it.isCompleteProperty }
                            }
                        }
                    }
                }
            }
            contextmenu {
                item("Open").action { StageEditor(selectedStage.item.stageTitle).openModal() }
                item("Delete").action {
                    val buttons = arrayOf(ButtonType.YES, ButtonType.NO)
                    val confirm = confirmation(header = "Confirm delete stage?", buttons = *buttons)
                    if (confirm.result == ButtonType.YES) {
                        projectsController.deleteStage(selectedStage.item)
                        val index = projectsController.projects.indexOf(selectedProject.item)
                        selectedProject.commit {
                            projectsController.projects[index] = selectedProject.item
                        }
                    }
                }
                item("Update effort spent").apply {
                    visibleWhen { selectedProject.isEffortEnabled }
                    action {
                        AddHours(selectedStage.item.stageTitle).openModal()
                    }
                }
                item("Mark Complete").apply {
                    disableWhen { selectedStage.isComplete }
                    action {
                        selectedStage.isComplete.value = true
                        val index = selectedProject.stages.indexOf(selectedStage.item)
                        selectedStage.commit {
                            selectedProject.stages[index] = selectedStage.item
                        }
                    }
                }
            }
            bindSelected(selectedStage)
        }
        borderpane {
            right {
                addClass(GlobalStyles.buttons_hbox_border)
                hbox {
                    button(" Open ").apply {
                        hboxConstraints {
                            marginLeft = 15.0
                            marginRight = 10.0
                            marginTop = 7.0
                            marginBottom = 5.0
                        }
                        disableProperty().bind(selectedStage.empty)
                        shortcut("ENTER")
                        action {
                            StageEditor(selectedStage.item.stageTitle).openModal()
                        }
                    }
                    button("Delete").apply {
                        hboxConstraints {
                            marginRight = 10.0
                            marginTop = 7.0
                            marginBottom = 5.0
                        }
                        disableProperty().bind(selectedStage.empty)
                        shortcut("DELETE")
                        action {
                            val buttons = arrayOf(ButtonType.YES, ButtonType.NO)
                            val confirm = confirmation(header = "Confirm delete stage?", buttons = *buttons)
                            if (confirm.result == ButtonType.YES) {
                                projectsController.deleteStage(selectedStage.item)
                                val index = projectsController.projects.indexOf(selectedProject.item)
                                selectedProject.commit {
                                    projectsController.projects[index] = selectedProject.item
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
