package com.mbsingh.pengine.appviews.project.stage

import com.mbsingh.pengine.appviews.styles.GlobalStyles
import com.mbsingh.pengine.models.objects.ProjectModel
import com.mbsingh.pengine.models.objects.ProjectStageModel
import javafx.geometry.Pos
import javafx.scene.control.Alert
import javafx.scene.text.Font
import tornadofx.*
import java.time.LocalDate

class StageEditor(stageName: String) : View(stageName) {

    private val selectedStage: ProjectStageModel by inject()
    private val selectedProject: ProjectModel by inject()

    override val root = borderpane {
        top {
            vbox {
                textfield {
                    alignment = Pos.CENTER_LEFT
                    prefHeight = 25.0
                    font = Font.font(25.0)
                    vboxConstraints {
                        marginLeft = 20.0
                        marginRight = 20.0
                    }
                    bind(selectedStage.stageTitle)
                }
                textarea().addClass(GlobalStyles.description_edit).apply {
                    promptText = "Description"
                    vboxConstraints {
                        prefHeight = 30.0
                        marginTop = 5.0
                        prefHeight = 30.0
                        marginLeft = 20.0
                        marginRight = 20.0
                    }
                    bind(selectedStage.description)
                }
                checkbox("Completed").apply {
                    borderpaneConstraints { marginRight = 20.0 }
                    bind(selectedStage.isComplete)
                    vboxConstraints {
                        marginTop = 12.0
                        marginLeft = 275.0
                        marginBottom = 7.0
                    }
                }
            }
        }
        center {
            borderpane {
                left {
                    hbox {
                        label("Due by").apply {
                            hboxConstraints {
                                marginTop = 5.0
                                marginLeft = 20.0
                            }
                        }
                        datepicker().apply {
                            hboxConstraints {
                                marginTop = 5.0
                                marginLeft = 5.0
                                prefWidth = 150.0
                                prefHeight = 30.0
                            }
                            bind(selectedStage.dueBy)
                        }
                    }
                }
                right {
                    hbox {
                        label("Estimated effort hours").apply {
                            hboxConstraints {
                                marginTop = 5.0
                            }
                            visibleWhen { selectedProject.isEffortEnabled }
                        }
                        textfield {
                            prefWidth = 70.0
                            filterInput { it.controlNewText.isInt() }
                            setOnKeyTyped { event -> if (text.length > 4) event.consume() }
                            visibleWhen { selectedProject.isEffortEnabled }
                            hboxConstraints {
                                marginTop = 5.0
                                marginLeft = 5.0
                                marginBottom = 7.0
                                marginRight = 20.0
                            }
                            bind(selectedStage.estimatedEffortHours)
                        }
                    }
                }
            }
        }
        bottom {
            hbox {
                button("Save & Close").apply {
                    hboxConstraints {
                        marginLeft = 175.0
                        marginTop = 5.0
                        marginRight = 20.0
                        marginBottom = 10.0
                    }
                    action {
                        selectedStage.description.value = selectedStage.description.value ?: ""
                        selectedStage.estimatedEffortHours.value = selectedStage.estimatedEffortHours.value ?: 0
                        if (selectedStage.stageTitle.value.isNullOrEmpty())
                            alert(Alert.AlertType.ERROR, "Stage title cannot be empty.")
                        else {
                            if (selectedStage.description.isNotDirty && selectedStage.isComplete.isNotDirty && selectedStage.stageTitle.isNotDirty &&
                                    selectedStage.estimatedEffortHours.isNotDirty && selectedStage.dueBy.isNotDirty)
                                alert(Alert.AlertType.ERROR, "No values modified")
                            else {
                                if (selectedStage.dueBy.value < LocalDate.now())
                                    alert(Alert.AlertType.ERROR, "Due By date must be today or later.")
                                else {
                                    if (selectedProject.item.isEffortEnabled) {
                                        if (selectedStage.estimatedEffortHours.value.toInt() < 1)
                                            alert(Alert.AlertType.ERROR, "Effort hours cannot be less than 1.")
                                        else {
                                            val index = selectedProject.stages.indexOf(selectedStage.item)
                                            selectedStage.commit {
                                                selectedProject.stages[index] = selectedStage.item
                                            }
                                            close()
                                        }
                                    } else {
                                        val index = selectedProject.stages.indexOf(selectedStage.item)
                                        selectedStage.commit {
                                            selectedProject.stages[index] = selectedStage.item
                                        }
                                        close()
                                    }
                                }
                            }
                        }
                    }
                }
                button("Reset").apply {
                    hboxConstraints {
                        marginTop = 5.0
                        marginRight = 20.0
                        marginBottom = 5.0
                    }
                    action {
                        selectedStage.rollback()
                    }
                }
                button("Cancel").apply {
                    hboxConstraints {
                        marginTop = 5.0
                        marginBottom = 5.0
                    }
                    action {
                        close()
                    }
                }
            }
        }
        primaryStage.setOnCloseRequest { event -> event.consume() }
    }
}
