package com.mbsingh.pengine.appviews.project.stage

import com.mbsingh.pengine.models.objects.ProjectModel
import com.mbsingh.pengine.models.objects.ProjectStageModel
import javafx.scene.control.Alert
import tornadofx.*

class AddHours(stageTitle: String) : View("Add effort spent on $stageTitle") {

    private val selectedProject: ProjectModel by inject()
    private val selectedStage: ProjectStageModel by inject()

    override val root = borderpane {
        center {
            vbox {
                textfield {
                    prefWidth = 70.0
                    filterInput { it.controlNewText.isInt() }
                    setOnKeyTyped { event -> if (text.length > 4) event.consume() }
                    vboxConstraints {
                        marginLeft = 15.0
                        marginRight = 15.0
                        marginTop = 10.0
                        marginBottom = 5.0
                    }
                    bind(selectedStage.effortHoursSpent)
                }
                hbox {
                    button("Update & Close").apply {
                        hboxConstraints {
                            marginBottom = 10.0
                            marginLeft = 7.0
                            marginRight = 5.0
                        }
                        action {
                            selectedStage.effortHoursSpent.value = selectedStage.effortHoursSpent.value ?: 0
                            if (selectedStage.effortHoursSpent.value.toInt() < 1)
                                alert(Alert.AlertType.ERROR, "Effort hours spent cannot be less than 1")
                            else {
                                if (selectedStage.effortHoursSpent.isNotDirty)
                                    alert(Alert.AlertType.ERROR, "Value not modified")
                                else {
                                    val index = selectedProject.stages.indexOf(selectedStage.item)
                                    selectedStage.commit {
                                        selectedProject.stages[index] = selectedStage.item
                                    }
                                    close()
                                }
                            }
                        }
                    }
                    button("Cancel").apply {
                        action { close() }
                        hboxConstraints { marginRight = 7.0 }
                    }
                }
            }
        }
        primaryStage.setOnCloseRequest { event -> event.consume() }
    }
}
