package com.mbsingh.pengine.appviews.project

import com.mbsingh.pengine.appviews.styles.GlobalStyles
import com.mbsingh.pengine.controller.ProjectController
import com.mbsingh.pengine.models.objects.ProjectModel
import javafx.beans.binding.Bindings
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.Alert
import javafx.scene.layout.Priority
import tornadofx.*
import java.time.LocalDate

class AddStageView: View() {

    private val projectsController: ProjectController by inject()
    private val selectedProject: ProjectModel by inject()
    private val stageTitle = SimpleStringProperty()
    private val effortHours = SimpleIntegerProperty()
    private val dueBy = SimpleObjectProperty<LocalDate>()

    override val root = form {
        hbox {
            val titleField = textfield()
            titleField.bind(stageTitle)
            titleField.promptText = "Stage Title"
            titleField.apply {
                hgrow = Priority.ALWAYS
                hboxConstraints {
                    prefWidth = 500.0
                    marginLeft = 20.0
                    marginRight = 25.0
                }
            }
            val effortField = textfield()
            effortField.bind(effortHours)
            effortField.prefWidth = 70.0
            effortField.text = ""
            effortField.promptText = "Hours"
            effortField.filterInput { it.controlNewText.isInt() }
            effortField.setOnKeyTyped { event ->
                if (effortField.text.length > 4) event.consume()
            }
            effortField.enableWhen { selectedProject.isEffortEnabled }
            val dueByField = datepicker()
            dueByField.promptText = "Due By"
            dueByField.bind(dueBy)
            dueByField.apply {
                hboxConstraints {
                    marginLeft = 25.0
                    prefWidth = 150.0
                    prefHeight = 30.0
                }
            }
            button("Add Stage").addClass(GlobalStyles.add_new_button).apply {
                hboxConstraints {
                    marginLeft = 30.0
                    marginRight = 20.0
                }
                disableProperty().bind(Bindings.isEmpty(titleField.textProperty()).or(Bindings.isEmpty(dueByField.editor.textProperty())).
                or(Bindings.isEmpty(effortField.textProperty()).and((effortField.isDisabled).not())))
                action {
                    if (dueBy.value < LocalDate.now())
                        alert(Alert.AlertType.ERROR, "Due By date must be today or later.")
                    else {
                        if (selectedProject.item.isEffortEnabled) {
                            if (effortField.textProperty().isEmpty.value || effortHours.value < 1)
                                alert(Alert.AlertType.ERROR, "Effort hours cannot be less than 1.")
                            else {
                                projectsController.addNewStage(stageTitle.value, effortHours.value, dueBy.value)
                                titleField.clear()
                                effortField.clear()
                                dueByField.editor.clear()
                                val index = projectsController.projects.indexOf(selectedProject.item)
                                selectedProject.commit {
                                    projectsController.projects[index] = selectedProject.item
                                }
                            }
                        } else {
                            projectsController.addNewStage(stageTitle.value, effortHours.value, dueBy.value)
                            titleField.clear()
                            effortField.clear()
                            dueByField.editor.clear()
                            val index = projectsController.projects.indexOf(selectedProject.item)
                            selectedProject.commit {
                                projectsController.projects[index] = selectedProject.item
                            }
                        }
                    }
                }
            }
        }
    }
}