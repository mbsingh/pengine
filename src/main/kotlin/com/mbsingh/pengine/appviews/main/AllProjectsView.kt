package com.mbsingh.pengine.appviews.main

import tornadofx.*

class AllProjectsView: View("PEngine") {

    override val root = borderpane {
        top(AllProjectsList::class)
        bottom(AddProjectView::class)
    }
}