package com.mbsingh.pengine.appviews.main

import com.mbsingh.pengine.appviews.project.ProjectView
import com.mbsingh.pengine.appviews.styles.GlobalStyles
import com.mbsingh.pengine.controller.ProjectController
import com.mbsingh.pengine.models.objects.ProjectModel
import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import javafx.stage.StageStyle
import tornadofx.*

class AllProjectsList : View() {

    private val projectsController: ProjectController by inject()
    private val selectedProject: ProjectModel by inject()

    override val root = vbox {
        minHeight = 50.0
        listview(projectsController.projects).addClass(GlobalStyles.list_view).apply {
            style { backgroundInsets += box(0.px) }
            onUserSelect {
                projectsController.initOpenProject(selectedProject.item)
                close()
                ProjectView(selectedProject.item.projectTitle).openWindow(stageStyle = StageStyle.UNDECORATED, escapeClosesWindow = false)
            }
            cellFormat {
                graphic = borderpane {
                    center {
                        hbox {
                            label(it.projectTitle).addClass(GlobalStyles.list_label)
                        }
                    }
                    right {
                        vbox {
                            hbox {
                                val done_total = projectsController.fetchStageCountString(item).split(" ")
                                label("${done_total[0]} ") {
                                    font = Font.font("Times New Roman", FontWeight.BOLD, 15.0)
                                    hboxConstraints { marginBottom = 2.0; marginTop = 2.0 }
                                }
                                label("/ ") {
                                    hboxConstraints { marginBottom = 2.0; marginTop = 2.0 }
                                }
                                label("${done_total[1]} ") {
                                    font = Font.font("Times New Roman", FontWeight.BOLD, 15.0)
                                    hboxConstraints { marginBottom = 2.0; marginTop = 2.0 }
                                }
                                label(" stages completed") {
                                    hboxConstraints { marginBottom = 2.0; marginTop = 2.0 }
                                }
                            }
                            label("COMPLETED").addClass(GlobalStyles.completed_label).apply {
                                visibleWhen { it.isCompleteProperty }
                            }
                        }
                    }
                }
            }
            contextmenu {
                item("Open").action {
                    projectsController.initOpenProject(selectedProject.item)
                    close()
                    ProjectView(selectedProject.item.projectTitle).openWindow(stageStyle = StageStyle.UNDECORATED, escapeClosesWindow = false)
                }
                item("Delete").action { projectsController.deleteProject(selectedProject.item) }
                item("Mark Complete").apply {
                    disableWhen { selectedProject.isComplete }
                    action {
                        selectedProject.isComplete.value = true
                        val index = projectsController.projects.indexOf(selectedProject.item)
                        selectedProject.commit {
                            projectsController.projects[index] = selectedProject.item
                        }
                    }
                }
            }
            bindSelected(selectedProject)
        }
        borderpane {
            right {
                addClass(GlobalStyles.buttons_hbox_border)
                hbox {
                    button(" Open ").apply {
                        hboxConstraints {
                            marginLeft = 15.0
                            marginRight = 10.0
                            marginTop = 7.0
                            marginBottom = 5.0
                        }
                        disableProperty().bind(selectedProject.empty)
                        shortcut("ENTER")
                        action {
                            projectsController.initOpenProject(selectedProject.item)
                            close()
                            ProjectView(selectedProject.item.projectTitle).openWindow(stageStyle = StageStyle.UNDECORATED, escapeClosesWindow = false)
                        }
                    }
                    button("Delete").apply {
                        hboxConstraints {
                            marginRight = 10.0
                            marginTop = 7.0
                            marginBottom = 5.0
                        }
                        disableProperty().bind(selectedProject.empty)
                        shortcut("DELETE")
                        action {
                            projectsController.deleteProject(selectedProject.item)
                        }
                    }
                }
            }
        }
    }
}