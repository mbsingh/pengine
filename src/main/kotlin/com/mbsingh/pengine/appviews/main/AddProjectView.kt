package com.mbsingh.pengine.appviews.main

import com.mbsingh.pengine.appviews.styles.GlobalStyles
import com.mbsingh.pengine.controller.ProjectController
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import tornadofx.*

class AddProjectView: View() {

    private val projectsController: ProjectController by inject()
    private val projectTitle = SimpleStringProperty()
    private val isEffortEnabled = SimpleBooleanProperty()

    override val root = hbox {
        useMaxWidth = true
        alignment = Pos.BOTTOM_CENTER
        hboxConstraints {
            marginTop = 10.0
            marginBottom = 10.0
        }
        form {
            hbox {
                val titleField = textfield()
                titleField.bind(projectTitle)
                titleField.prefWidth = 500.0
                titleField.promptText = "Project Title"
                titleField.apply { hboxConstraints { marginLeft = 20.0 } }
                val effortField = checkbox("Effort Tracking?")
                effortField.bind(isEffortEnabled)
                effortField.apply { hboxConstraints {
                    marginLeft = 30.0
                    marginRight = 30.0
                } }
                button("Add Project").addClass(GlobalStyles.add_new_button).apply {
                    hboxConstraints { marginRight = 20.0 }
                    disableProperty().bind(titleField.textProperty().isEmpty)
                    action {
                        projectsController.addNewProject(projectTitle.value, isEffortEnabled.value)
                        titleField.clear()
                        effortField.isSelected = false
                    }
                }
            }
        }
    }
}