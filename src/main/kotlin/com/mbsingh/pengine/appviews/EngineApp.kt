package com.mbsingh.pengine.appviews

import com.mbsingh.pengine.appviews.main.AllProjectsView
import com.mbsingh.pengine.appviews.styles.GlobalStyles
import javafx.scene.image.Image
import javafx.stage.Stage
import tornadofx.*

class EngineApp: App(AllProjectsView::class, GlobalStyles::class) {

    override fun start(stage: Stage) {
        super.start(stage)
        stage.setOnCloseRequest {  }
        stage.minWidth = 700.0
        stage.icons.add(Image("resources/logo/pe.png"))
    }
}
